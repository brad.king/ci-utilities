#!/bin/sh

# Run this script on a linux host to generate dawn binaries.

set -e
set -x

target_arch="$WASM_ARCHITECTURE"
cmake_c_flags=""
cmake_cxx_flags=""
case "$target_arch" in
    wasm32-emscripten)
        ;;
    wasm64-emscripten)
        cmake_c_flags="-sMEMORY64=1"
        cmake_cxx_flags="-sMEMORY64=1"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly target_arch
readonly cmake_c_flags

# Keep in sync with the Windows script.
readonly git_url="https://dawn.googlesource.com/dawn"
# Dawn doesn't have any version tags. Instead the tags refer to chromium's patch version.
readonly chromium_patch_version="7037"
readonly git_tag="chromium/$chromium_patch_version"

git clone --depth=1 --branch "$git_tag" "$git_url" dawn/src

here="$( pwd )"
readonly here

# Configure and build
emcmake cmake \
  -GNinja \
  -DCMAKE_C_FLAGS="$cmake_c_flags" \
  -DCMAKE_CXX_FLAGS="$cmake_cxx_flags" \
  -C "dawn/dawn.emscripten.cmake" \
  -S "dawn/src" \
  -B "dawn/build" \
  -DDAWN_EMSCRIPTEN_TOOLCHAIN="${EMSDK}/upstream/emscripten"
cmake --build "dawn/build" --target emdawnwebgpu_cpp emdawnwebgpu_c

# manually install the generated JS and headers
dawn_install_prefix="$here/install/dawn-$chromium_patch_version-$target_arch"
mkdir -p "$dawn_install_prefix/lib"
cp "dawn/build/gen/src/emdawnwebgpu/library_webgpu_enum_tables.js" "$dawn_install_prefix/lib"
cp "dawn/build/gen/src/emdawnwebgpu/library_webgpu_generated_sig_info.js" "$dawn_install_prefix/lib"
cp "dawn/build/gen/src/emdawnwebgpu/library_webgpu_generated_struct_info.js" "$dawn_install_prefix/lib"
cp "dawn/build/src/emdawnwebgpu/libemdawnwebgpu_c.a" "$dawn_install_prefix/lib"
cp "dawn/src/third_party/emdawnwebgpu/library_webgpu.js" "$dawn_install_prefix/lib"
cp "${EMSDK}/upstream/emscripten/src/closure-externs/webgpu-externs.js" "$dawn_install_prefix/lib"

mkdir -p "$dawn_install_prefix/include/webgpu"
cp "dawn/build/gen/src/emdawnwebgpu/include/webgpu/webgpu_cpp.h" "$dawn_install_prefix/include/webgpu"
cp "dawn/build/gen/src/emdawnwebgpu/include/webgpu/webgpu.h" "$dawn_install_prefix/include/webgpu"
cp "dawn/build/gen/src/emdawnwebgpu/include/webgpu/webgpu_cpp_chained_struct.h" "$dawn_install_prefix/include/webgpu"
cp "dawn/src/include/webgpu/webgpu_enum_class_bitmasks.h" "$dawn_install_prefix/include/webgpu"

mkdir -p "$dawn_install_prefix/lib/cmake/emdawnwebgpu"
cp "dawn/emdawnwebgpu-config.cmake" "$dawn_install_prefix/lib/cmake/emdawnwebgpu"

# Create the final tarball containing the binaries.
tar cvzf \
  "dawn-$chromium_patch_version-$target_arch.tar.gz" \
  -C "$here/install" \
  "dawn-$chromium_patch_version-$target_arch"

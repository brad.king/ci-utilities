set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")

##############
# Dawn options
##############
set(DAWN_BUILD_SAMPLES OFF CACHE BOOL "")
set(DAWN_ENABLE_INSTALL OFF CACHE BOOL "")
set(DAWN_FETCH_DEPENDENCIES ON CACHE BOOL "")

##############
# Tint options
##############
set(TINT_BUILD_CMD_TOOLS OFF CACHE BOOL "")
set(TINT_BUILD_GLSL_VALIDATOR OFF CACHE BOOL "") # not needed because OpenGL is disabled
set(TINT_BUILD_GLSL_WRITER OFF CACHE BOOL "") # not needed because OpenGL is disabled
set(TINT_BUILD_IR_BINARY OFF CACHE BOOL "")
set(TINT_BUILD_TESTS OFF CACHE BOOL "")

# Keep in sync with the Windows script.
readonly git_url='https://github.com/Kitware/ninja.git'
readonly git_commit='95dee2a91d96c409d54f9fa0b70ea9aa2bdf8e63' # v1.11.1.g95dee.kitware.jobserver-1
readonly version='1.11.1.g95dee.kitware.jobserver-1'

git clone "$git_url" ninja/src
git -C ninja/src -c advice.detachedHead=false checkout "$git_commit"

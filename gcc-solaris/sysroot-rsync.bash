#!/usr/bin/env bash

set -e

readonly usage='sysroot-rsync.bash <src> <triple> [<rpath-flags>...]

Examples:

  sysroot-rsync.bash some-host:/     sparc-sun-solaris2.10
  sysroot-rsync.bash /local/archive/ i386-pc-solaris2.10
'

die() {
    echo "$@" 1>&2; exit 1
}

src="$1"; shift
triple="$1"; shift
test -n "$src" -a -n "$triple" || die "$usage"

mkdir -p "sysroot/$triple"
rsync -av "$@" \
  --files-from="${BASH_SOURCE%/*}/sysroot-paths.lst" \
  --ignore-missing-args \
  "$src" "sysroot/$triple/"

#!/usr/bin/env bash

set -e

arch="$1"
readonly arch

case "$arch" in
    i386)
        target=i386-pc-solaris2.10
        lib_arch32=
        lib_arch64=/amd64
        lib_arch_default=$lib_arch32
        sysroot_tarball="sysroot-i386-pc-solaris2.10-sunos5.10-1.tar.xz"
        sysroot_sha256sum="1b9251699f4e412ba5b0fde9c0fb96ceef6b8a1f47f0c1f2146ba0ba9da458b8"
        ;;
    x86_64)
        target=x86_64-pc-solaris2.10
        lib_arch32=
        lib_arch64=/amd64
        lib_arch_default=$lib_arch64
        sysroot_tarball="sysroot-x86_64-pc-solaris2.10-sunos5.10-1.tar.xz"
        sysroot_sha256sum="bea632b3ae755f89a1c0e64775437a9b29001a3fc3a3c2c6247b921776059231"
        ;;
    sparc)
        target=sparc-sun-solaris2.10
        lib_arch32=
        lib_arch64=/sparcv9
        lib_arch_default=$lib_arch32
        sysroot_tarball="sysroot-sparc-sun-solaris2.10-sunos5.10-1.tar.xz"
        sysroot_sha256sum="e6c668a63dc00de443d07cbe2be779335642ffe1b818ba85d23ab543982aaf23"
        ;;
    sparc64)
        target=sparc64-sun-solaris2.10
        lib_arch32=
        lib_arch64=/sparcv9
        lib_arch_default=$lib_arch64
        sysroot_tarball="sysroot-sparc64-sun-solaris2.10-sunos5.10-1.tar.xz"
        sysroot_sha256sum="fd60cc1be951ae314ff2b4246ac055c8e5b21c39b4cd41b23ebcec709451d90f"
        ;;
    *)
        echo >&2 "Unknown architecture: $arch"
        exit 1
        ;;
esac
readonly target
readonly sysroot_tarball
readonly sysroot_sha256sum

readonly binutils_filename="binutils-2.44"
readonly binutils_sha256sum="ce2017e059d63e67ddb9240e9d4ec49c2893605035cd60e92ad53177f4377237"

readonly gcc_filename="gcc-9.5.0"
readonly gcc_sha256sum="27769f64ef1d4cd5e2be8682c0c93f9887983e6cfd1a927ce5a0a2915a95cf8f"

apt-get update
apt-get install -y --no-install-recommends \
  ca-certificates \
  curl \
  file \
  g++ \
  gcc \
  libc-dev \
  libgmp-dev \
  libmpc-dev \
  libmpfr-dev \
  m4 \
  make \
  patch \
  texinfo \
  xz-utils \
  #

# sysroot
echo "$sysroot_sha256sum  $sysroot_tarball" > sysroot.sha256sum
curl -OL "https://cmake.org/files/dependencies/internal/sunos/$sysroot_tarball"
sha256sum --check sysroot.sha256sum
mkdir -p /opt/cross
tar xJf "$sysroot_tarball" -C /opt/cross

# binutils
readonly binutils_tarball="$binutils_filename.tar.xz"
echo "$binutils_sha256sum  $binutils_tarball" > binutils.sha256sum
curl -OL "https://ftp.gnu.org/gnu/binutils/$binutils_tarball"
sha256sum --check binutils.sha256sum
tar xJf "$binutils_tarball"
mkdir "$binutils_filename/build"
pushd "$binutils_filename/build"
../configure --prefix="/opt/cross" --target="$target"
make -j $(nproc)
make install
popd

# gcc
gcc_tarball="$gcc_filename.tar.xz"
echo "$gcc_sha256sum  $gcc_tarball" > gcc.sha256sum
curl -OL "https://ftp.gnu.org/gnu/gcc/$gcc_filename/$gcc_tarball"
sha256sum --check gcc.sha256sum
tar xJf "$gcc_tarball"
(cd "$gcc_filename" && patch -p0) < "${BASH_SOURCE%/*}/gcc.patch"
mkdir "$gcc_filename/build"
pushd "$gcc_filename/build"
../configure \
  --prefix="/opt/cross" \
  --target="$target" \
  --with-sysroot="/opt/cross/sysroot/$target" \
  --with-gnu-as \
  --with-gnu-ld \
  --enable-languages=c,c++,fortran,lto,objc \
  --enable-obsolete
make -j $(nproc)
make install
popd
readonly gcc="/opt/cross/bin/$target-gcc"
readonly specs="$(dirname $("$gcc" -print-libgcc-file-name))/specs"
"$gcc" -dumpspecs > "$specs"
sed -i '
  /^\*link_arch32:$/ {n;s|$| -rpath-link %R/usr/lib'"$lib_arch32"' -rpath-link %R/lib'"$lib_arch32"'|}
  /^\*link_arch64:$/ {n;s|$| -rpath-link %R/usr/lib'"$lib_arch64"' -rpath-link %R/lib'"$lib_arch64"'|}
  /^\*link_arch_default:$/ {n;s|$| -rpath-link %R/usr/lib'"$lib_arch_default"' -rpath-link %R/lib'"$lib_arch_default"'|}
' "$specs"

cat >"/opt/cross/README.$target" <<EOF
Populate the sysroot:

  /opt/cross/sysroot/$target

with copies of the following paths from a real Solaris $arch host:

  lib
  usr/lib
  usr/include

Optionally add:

  usr/openwin/lib
  usr/openwin/include
  usr/openwin/share/include

  usr/X11/lib
  usr/X11/include

  usr/dt/lib
  usr/dt/include
  usr/dt/share/include
EOF

tar cJf "gcc-9.5.0-linux-x86_64-cross-sunos-$arch.tar.xz" -C / --exclude=opt/cross/sysroot opt/cross

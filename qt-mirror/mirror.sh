#!/bin/sh

set -e

# Downloads Qt binaries from Qt's website for internal mirroring purposes.

cd "download.qt.io/"
wget2 \
    --no-metalink \
    --input-file=../qtdirs \
    --no-host-directories \
    --content-disposition \
    --span-hosts \
    --continue \
    --recursive \
    --level=1 \
    --accept=.7z \
    --accept=.7z.meta4

#!/bin/bash

set -e

readonly git_url='https://gitlab.kitware.com/utils/ghostflow-director.git'
readonly git_commit='4007b9ef4ccb1744c7fa4d694538e2dc227c3d8f' # skip-sync-merge-if-already-merged
readonly cargo_features="--features systemd"

git clone "$git_url" ghostflow-director/src
pushd ghostflow-director/src
git -c advice.detachedHead=false checkout "$git_commit"
short_commit="$( git rev-parse --short "$git_commit" )"
readonly short_commit
cargo build $cargo_features
cargo run $cargo_features -- --version
popd
mv ghostflow-director/src/target/debug/ghostflow-director "ghostflow-director-$short_commit"

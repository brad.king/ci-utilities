#!/bin/sh

set -e

readonly patchdir="$1"
shift

# Cross compilation is not supported by this script.
# Make sure SDKROOT env var points to the right SDK
# - 10.13 for x86_64
# - 11.0 for aarch64
readonly target_arch="$( uname -m )"

case "$target_arch" in
    x86_64)
        min_macosx_version="10.13"
        arch="x86_64"
        target="$arch-apple-darwin"
        ;;
    arm64)
        min_macosx_version="11.0"
        arch="aarch64"
        target="$arch-apple-darwin"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly min_macosx_version
readonly arch
readonly target

readonly git_url='https://gitlab.kitware.com/paraview/paraview-superbuild'
readonly git_commit='77c860a7bb9d7d7f8b8a3d067b99aac92ab3bce8'

mkdir -p pvsb
cd pvsb
git clone "$git_url" src
cd src
git checkout "$git_commit"
git submodule update --init --recursive
cd ../
mkdir build
cd build
cmake \
  -DENABLE_nlohmannjson=OFF \
  -DENABLE_paraview=OFF \
  -DENABLE_cdi=ON \
  -DCMAKE_OSX_DEPLOYMENT_TARGET="$min_macosx_version" \
  ../src
make
patch ./install/lib/cmake/cdi/cdi-config.cmake < $patchdir/cdi-config.patch
install_name_tool -change `pwd`/install/lib/libz.1.dylib @loader_path/libz.1.dylib ./install/lib/libhdf5.310.dylib
install_name_tool -change `pwd`/install/lib/libsz.2.dylib @loader_path/libsz.2.dylib ./install/lib/libhdf5.310.dylib
install_name_tool -change `pwd`/install/lib/libhdf5.310.dylib @loader_path/libhdf5.310.dylib ./install/lib/libhdf5_hl.310.dylib
install_name_tool -change `pwd`/install/lib/libhdf5.310.dylib @loader_path/libhdf5.310.dylib ./install/lib/libnetcdf.19.dylib
install_name_tool -change `pwd`/install/lib/libhdf5_hl.310.dylib @loader_path/libhdf5_hl.310.dylib ./install/lib/libnetcdf.19.dylib
install_name_tool -change `pwd`/install/lib/libhdf5.310.dylib @loader_path/libhdf5.310.dylib ./install/lib/libcdi.0.dylib
install_name_tool -change `pwd`/install/lib/libnetcdf.19.dylib @loader_path/libnetcdf.19.dylib ./install/lib/libcdi.0.dylib
install_name_tool -id @rpath/libcdi.0.dylib ./install/lib/libcdi.0.dylib
mv ./install ./cdi
tar -czvf ../../cdi-macos$min_macosx_version-$arch.tar.gz ./cdi
